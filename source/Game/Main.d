﻿module Game.Main;

private
{
	import Game.Util, Game.Config, Game.Loader, Game.GameState, Game.Keyboard,
		Game.Mouse, Game.Item, Game.Network;
}

class Main
{
	private
	{
		sfRenderWindow*		_Window;
		GameStateManager	_StateManager;

		void AddStates()
		{
			this._StateManager.Add!TestState();
			this._StateManager.Add!MainMenu();
			this._StateManager.Add!PlayScreen();
		}
	}

	public
	{
		this()
		{
			Util.InitDerelict();
			Loader.LoadFont();
			ItemLoader.LoadExtension("Test.json");

			this._StateManager = new GameStateManager();

			this.AddStates();
			this._StateManager.SwapTo("Menu");
		}

		/// Start looping the game.
		void Loop()
		{
			Util.LogSender = "Main::Loop";
			Util.Log("Creating window and clock.");

			this._Window = sfRenderWindow_create(Config.Window.VideoMode, cast(const(char)*)Config.Window.Title.ptr, sfClose, null);
			sfRenderWindow_setFramerateLimit(this._Window, 60);
		
			sfEvent 	e;
			sfTime 		Time;
			sfClock*	Clock = sfClock_create();

			while(sfRenderWindow_isOpen(this._Window))
			{
				Time = sfClock_restart(Clock);
				Keyboard.Clear();
				Mouse.HandleMousePosition(this._Window);

				// Event stuff
				while(sfRenderWindow_pollEvent(this._Window, &e))
				{
					// Register the keys pressed.
					Keyboard.HandleKeyInput(e);

					// Handle mouse stuff
					Mouse.HandleMouseInput(e);

					if(e.type == sfEvtClosed)
					{
						sfRenderWindow_close(this._Window);
					}
				}
				sfRenderWindow_clear(this._Window, sfWhite);
				Network.Update();

				this._StateManager.Update(Time);
				this._StateManager.Draw(this._Window);

				sfRenderWindow_display(this._Window);
			}

			Util.LogSender = "Main::Loop";
			Util.Log("Destroying window and clock.");

			sfRenderWindow_destroy(this._Window);
			sfClock_destroy(Clock);
		}
	}
}