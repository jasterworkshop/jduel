﻿module Game.Item;

private
{
	import std.json;
	import std.file : readText;

	import Game.Player;
}

public
{
	import Game.Drawable, Game.Loader, Game.Config, Game.Keyboard, Game.Mouse, Game.Animation;
}

struct ItemInfo
{
	uint			ID;
	string 			Type;
	string 			Name;
	string			Texture;
	StatInfo		Bonuses;

	/// Exclusive to "Weapon" types. Name of it's attacks.
	string[]		Attacks;
}

struct AttackInfo
{
	string			Name;

	/// How many pixels the animation projectile moves per loop towards the enemy, aka, how long it stays on screen.
	uint			Speed;

	/// What effect the attack has on the enemy's stats.
	StatInfo		Damage;

	/// Describes how to animate the attack's frame-sheet
	AnimationInfo	Animation;
}

class Item : Drawable
{
	private
	{
		ItemInfo	_Info;
		sfSprite*	_Sprite;

		/// Used by Weapon types
		Attack[]	_Attacks;
		sfVector2f	_Position;
		Player		_Owner;

		uint		_Attack;
	}

	public
	{
		this(ItemInfo info, sfVector2f position, Player owner)
		{
			this._Position = position;
			this._Owner = owner;
			this._Info = info;
			this.Init();
		}

		override
		{
			void Init()
			{
				this._Sprite = sfSprite_create();
				sfSprite_setTexture(this._Sprite, Loader.LoadTexture(this._Info.Texture), true);
				sfSprite_setPosition(this._Sprite, this._Position);

				if(this._Owner.IsEnemy)
				{
					sfSprite_setScale(this._Sprite, sfVector2f(-1, 1));
				}

				if(this._Info.Type == "Weapon")
				{
					foreach(attack; this._Info.Attacks)
					{
						this._Attacks ~= Game.Item.Attacks.Construct(attack, this._Position, this._Owner, this._Attacks.length);
					}
				}
			}

			void Draw(sfRenderWindow* window)
			{
				sfRenderWindow_drawSprite(window, this._Sprite, null);
				if(this._Info.Type == "Weapon" && this._Attacks.length > 0) this._Attacks[this._Attack].Draw(window);
			}

			void Update(sfTime gameTime)
			{
				if(this._Info.Type == "Weapon" && this._Attacks.length > 0) this._Attacks[this._Attack].Update(gameTime);
			}
		}

		void DoAttack()
		{
			if(this._Info.Type == "Weapon" && this._Attacks.length > 0)
			{
				this._Attacks[this._Attack].Attack();
			}
		}

		void SetAttack(uint id)
		{
			Util.LogSender = "Item::SetAttack";
			Util.Log("Setting attack to %d.", id);

			this._Attack = id;
		}

		Attack GetAttack(uint id)
		{
			return this._Attacks[id];
		}

		@property
		bool AnimatingAttack()
		{
			return this._Attacks[this._Attack].Attacking;
		}

		@property
		StatInfo Bonuses()
		{
			return this._Info.Bonuses;
		}

		@property
		Attack[] Attacks()
		{
			return this._Attacks;
		}
	}
}

static class ItemLoader
{
	private static
	{
		ItemInfo[uint]	_Items;
	}

	public static
	{
		Item Construct(uint id, sfVector2f position, Player owner)
		{
			Util.LogSender = "ItemLoader::Construct";
			Util.Log("Constructing item with id of %d at position %s.", id, position);

			return new Item(this._Items[id], position, owner);
		}

		ItemInfo[] GetItemsOfType(string type)
		{
			ItemInfo[] ToReturn;

			foreach(item; this._Items.values)
			{
				if(item.Type == type)
				{
					ToReturn ~= item;
				}
			}

			return ToReturn;
		}

		void LoadExtension(string path)
		{
			Util.LogSender = "ItemLoader::LoadExtension";
			Util.Log("Loading extension '%s'.", path);

			JSONValue[] Data = parseJSON(readText(Config.AssetDirectories.Items ~ path))["Data"].array;
		
			string[] ToStringArray(JSONValue[] json)
			{
				string[] ToReturn;

				foreach(n; json)
				{
					ToReturn ~= n.str;
				}

				return ToReturn;
			}

			foreach(node; Data)
			{
				ItemInfo 	Info;
				auto 		DataNode = node.object;
				bool		Continue = false;

				switch(DataNode["Type"].str)
				{
					case "Weapon":
						Info.Bonuses = ParseBonus(DataNode["Bonuses"].object);
						Info.Attacks = ToStringArray(DataNode["Attacks"].array);
						break;

					case "Attack":
						Attacks.CreateAttack(DataNode);
						Util.LogSender = "ItemLoader::LoadExtension";
						Continue = true;
						break;

					default:
						Info.Bonuses = ParseBonus(DataNode["Bonuses"].object);
						break;
				}
				if(Continue) continue;

				Info.Type		= DataNode["Type"].str;
				Info.Name 		= DataNode["Name"].str;
				Info.Texture 	= DataNode["Texture"].str;
				Info.ID			= cast(uint)DataNode["ID"].integer;

				this._Items[Info.ID] = Info;

				debug
				{
					Util.Log("[DEBUG] Registered Item: %s", Info);
				}
			}
		}
	}
}

class Attack : Drawable
{
	private
	{
		Animation	_Animation;
		AttackInfo	_Info;
		bool		_Attacking = false;

		uint		_ID;

		sfVector2f	_Origin;
		Player		_Owner;
	}

	public
	{
		this(AttackInfo info, sfVector2f origin, Player player, uint id)
		{
			this._ID = id;
			this._Info = info;
			this._Origin = origin;
			this._Owner = player;
			this.Init();
		}

		override
		{
			void Init()
			{
				this._Animation = new Animation(this._Info.Animation);

				if(this._Owner.IsEnemy)
				{
					this._Animation.Flip();
				}
			}

			void Draw(sfRenderWindow* window)
			{
				if(this._Attacking)
				{
					this._Animation.Draw(window);
				}
			}

			void Update(sfTime gameTime)
			{
				if(this._Attacking)
				{
					if(this.Position.x > Config.EnemyPosition.x || this.Position.x < Config.PlayerPosition.x)
					{
						Battle.ProcessAttack(this._ID);
						this.Reset();
						return;
					}

					this._Animation.Update(gameTime);

					if(this._Owner.IsEnemy)
					{
						this._Animation.Position = sfVector2f(this._Animation.Position.x - this._Info.Speed, this._Animation.Position.y);
					}
					else
					{
						this._Animation.Position = sfVector2f(this._Animation.Position.x + this._Info.Speed, this._Animation.Position.y);
					}
				}
			}
		}

		void Attack()
		{
			this._Attacking = true;
		}

		void Reset()
		{
			this._Attacking = false;
			this.Position = this._Origin;
			this._Animation.Reset();
		}

		/// Get whether the Attack is animating.
		@property
		bool Attacking()
		{
			return this._Attacking;
		}

		@property
		void Position(sfVector2f position)
		{
			this._Animation.Position = position;
		}

		@property
		sfVector2f Position()
		{
			return this._Animation.Position;
		}

		@property
		string Name()
		{
			return this._Info.Name;
		}

		@property
		StatInfo Damage()
		{
			return this._Info.Damage;
		}
	}
}

static class Attacks
{
	private static
	{
		AttackInfo[string]	_Attacks;
	}

	public static
	{
		void CreateAttack(JSONValue[string] node)
		{
			auto AniNode = node["Animation"].object;
			this._Attacks[node["Name"].str] = AttackInfo(node["Name"].str, cast(uint)node["Speed"].integer, ParseBonus(node["Damage"].object), AnimationInfo(cast(uint)AniNode["FrameWidth"].integer, cast(uint)AniNode["FrameHeight"].integer, 0, cast(uint)AniNode["FrameTimer"].integer, AniNode["Repeatable"].type == JSON_TYPE.TRUE, node["Texture"].str));
		
			debug
			{
				Util.LogSender = "Attacks::CreateAttack";
				Util.Log("[DEBUG] Created Attack: %s.", this._Attacks[node["Name"].str]);
			}
		}

		Attack Construct(string name, sfVector2f origin, Player owner, uint id)
		{
			Util.LogSender = "Attacks::Construct";
			Util.Log("Constructing attack with name '%s' at origin '%s'.", name, origin);

			return new Attack(this._Attacks[name], origin, owner, id);
		}
	}
}

private StatInfo ParseBonus(JSONValue[string] bonusNode)
{
	uint GetIfExists(JSONValue[string] node, string wanted, uint defult)
	{
		auto X = wanted in node;
		if(X is null)
		{
			return defult;
		}
		else
		{
			return cast(uint)(*X).integer;
		}
	}

	return StatInfo(
		GetIfExists(bonusNode, "Health", 0),
		GetIfExists(bonusNode, "PDefence", 0),
		GetIfExists(bonusNode, "MDefence", 0),
		GetIfExists(bonusNode, "PAttack", 0),
		GetIfExists(bonusNode, "MAttack", 0)
		);
}
