﻿module Game.Keyboard;

private
{


	import derelict.sfml2.window;

	import Game.Util;
}

static class Keyboard
{
	private static
	{
		dchar[]	_Pressed;
	}

	public static
	{
		/// Handles a key input event.
		void HandleKeyInput(sfEvent event)
		{
			if(event.type == sfEvtKeyPressed){}
			if(event.type == sfEvtTextEntered)
			{
				this._Pressed ~= event.text.unicode;
			}
		}

		/// Clears the keys pressed
		void Clear()
		{
			this._Pressed.length = 0;
		}

		/// Returns every key that has been pressed.
		@property
		dchar[] Pressed()
		{
			return this._Pressed;
		}
	}
}
