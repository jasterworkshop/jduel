﻿module Game.Loader;

public
{
	import std.string;

	import Game.Config, Game.Util;
}

static class Loader
{
	private static
	{
		sfTexture*[string]	_TextureCache;
		sfSound*[string]	_AudioCache;

		// I'm only ever gonna use a single font
		sfFont*				_Font;
	}

	public static
	{
		/// Loads the main font.
		void LoadFont()
		{
			Util.LogSender = "Loader::LoadFont";
			Util.Log("Loading main font '%s'.", Config.MainFont);

			this._Font = sfFont_createFromFile(cast(const(char)*)Config.MainFont.ptr);
		}

		~this()
		{
			Util.LogSender = "Loader::~this";
			Util.Log("Unloading cached assets.");

			foreach(texture; Loader._TextureCache.values)
			{
				sfTexture_destroy(texture);
			}

			foreach(audio; Loader._AudioCache.values)
			{
				sfSound_destroy(audio);
			}

			sfFont_destroy(Loader._Font);
		}

		/// Loads/caches the texture at a given path.
		/// 
		/// Parameters:
		/// 	path = The path under $(B Config.AssetDirectories.Textures) where the desired texture is.
		sfTexture* LoadTexture(string path)
		{
			Util.LogSender = "Loader::LoadTexture";
			Util.Log("Requested to load texture '%s'.", path);

			auto Texture = (path in Loader._TextureCache);

			if(Texture is null)
			{
				Util.Log("Texture not cached, loading and caching.");

				auto ToReturn 				= sfTexture_createFromFile((Config.AssetDirectories.Textures.dup ~ path).toStringz(), null);
				Loader._TextureCache[path] 	= ToReturn;

				return ToReturn;
			}
			else
			{
				Util.Log("Texture cached, returning.");

				return *Texture;
			}
		}

		/// Get the font used for drawing strings.
		@property
		sfFont* DrawingFont()
		{
			return this._Font;
		}
	}
}