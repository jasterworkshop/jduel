﻿module Game.GameState;

private
{
	import std.utf;
	import Game.Config, Game.Util, Game.Loader, Game.Drawable, Game.Button, Game.Keyboard, Game.Mouse, Game.Textbox, Game.Player, Game.Network;
}

abstract class GameState : Drawable
{
	private
	{
		GameStateManager	_Manager;
		string				_Name;
	}

	public
	{
		this(GameStateManager manager, string name)
		{
			this._Manager 	= manager;
			this._Name 		= name;
		}

		/// Get the GameState's name.
		@property
		string Name()
		{
			return this._Name;
		}

		/// Get the GameState's manager.
		@property
		GameStateManager Manager()
		{
			return this._Manager;
		}
	}
}

class GameStateManager
{
	private
	{
		// Game states are indexed by their "Name" fields.
		GameState[string]	_States;
		GameState			_CurrentState;
	}

	public
	{
		/// Add a given GameState to the manager.
		void Add(T : GameState)()
		{
			T State = new T(this);

			Util.LogSender = "GameStateManager::Add";
			Util.Log("Initialising and registering state with name of '%s'.", State.Name);

			State.Init();
			this._States[State.Name] = State;
		}

		/// Swaps the current GameState to the one with the given name.
		void SwapTo(string name)
		{
			Util.LogSender = "GameStateManager::SwapTo";

			Util.Log("Swapping current GameState to '%s'.", name);
			this._CurrentState = this._States[name];

			Util.Log("Clearing Keyboard buffer and Mouse flags.");
			Keyboard.Clear();
			Mouse.Clear();
		}

		/// Draw the current GameState.
		void Draw(sfRenderWindow* window)
		{
			if(this._CurrentState !is null)
			{
				this._CurrentState.Draw(window);
			}
		}

		/// Update the current GameState.
		void Update(sfTime gameTime)
		{
			if(this._CurrentState !is null)
			{
				this._CurrentState.Update(gameTime);
			}
		}
	}
}

class TestState : GameState
{
	private
	{
		Button	_Test;
		TextBox	_Box;
	
		void TestEvent(Button button, GameState _)
		{
			Util.LogSender = "TestState::TestEvent";
			Util.Log("Event worked.");
		}
	}

	public
	{
		this(GameStateManager manager)
		{
			super(manager, "Test");
		}

		override
		{
			void Init()
			{
				this._Test = new Button(this, &this.TestEvent, sfVector2f(250, 100), sfVector2f(50, 50), TextOptions(24, "This is a test button"d));
				this._Box = new TextBox(this, sfVector2f(250, 100), sfVector2f(50, 300), TextOptions(24, ""d));
			}

			void Draw(sfRenderWindow* window)
			{
				this._Test.Draw(window);
				this._Box.Draw(window);
				Battle.Draw(window);
			}

			void Update(sfTime gameTime)
			{
				this._Test.Update(gameTime);
				this._Box.Update(gameTime);
				Battle.Update(gameTime);
			}
		}
	}
}

class MainMenu : GameState
{
	private
	{
		const static sfVector2f	_ButtonSize = sfVector2f(100, 50);

		Button	_Host;
		Button	_Play;
		Button	_Join;

		debug
		{
			Button	_TestButton;

			void TestButtonHandle(Button sender, GameState parent)
			{
				Network.Send("Dan is not sexy!", Opcode.Message);
				Battle.SendPlayerToEnemy();
				this.Manager.SwapTo("Test");
			}
		}

		void HostHandle(Button sender, GameState parent)
		{
			Network.Host();
		}

		void JoinHandle(Button sender, GameState parent)
		{
			import std.stdio;
			Network.Connect(readln()[0..$-1]);
		}

		void PlayHandle(Button sender, GameState parent)
		{
			Battle.SendPlayerToEnemy();
			this.Manager.SwapTo("Play");
		}
	}
	
	public
	{
		this(GameStateManager manager)
		{
			super(manager, "Menu");
		}
		
		override
		{
			void Init()
			{
				debug
				{
					this._TestButton = new Button(this, &this.TestButtonHandle, this._ButtonSize, sfVector2f(Util.CenterX(this._ButtonSize.x), (Config.Window.VideoMode.height - this._ButtonSize.y) - 20), TextOptions(24, "Test"));
				}

				this._Host = new Button(this, &this.HostHandle, this._ButtonSize, sfVector2f(Util.CenterX(this._ButtonSize.x), (Config.Window.VideoMode.height - this._ButtonSize.y) - 80), TextOptions(24, "Host"));
				this._Join = new Button(this, &this.JoinHandle, this._ButtonSize, sfVector2f(Util.CenterX(this._ButtonSize.x), (Config.Window.VideoMode.height - this._ButtonSize.y) - 140), TextOptions(24, "Join"));
				this._Play = new Button(this, &this.PlayHandle, this._ButtonSize, sfVector2f(Util.CenterX(this._ButtonSize.x), (Config.Window.VideoMode.height - this._ButtonSize.y) - 200), TextOptions(24, "Play"));
			}
			
			void Draw(sfRenderWindow* window)
			{
				debug
				{
					this._TestButton.Draw(window);
				}

				this._Host.Draw(window);
				this._Join.Draw(window);
				this._Play.Draw(window);
			}
			
			void Update(sfTime gameTime)
			{
				debug
				{
					this._TestButton.Update(gameTime);
				}

				this._Host.Update(gameTime);
				this._Join.Update(gameTime);
				this._Play.Update(gameTime);
			}
		}
	}
}


class PlayScreen : GameState
{
	private
	{
		Button	_LastAttack;

		uint[5] LoadPlayerData()
		{
			import std.json, std.file;

			uint[5] ToReturn;
			foreach(i, val; parseJSON(readText(Config.PlayerData))["Gear"].array) ToReturn[i] = cast(uint)val.integer;

			return ToReturn;
		}
	}
	
	public
	{
		this(GameStateManager manager)
		{
			super(manager, "Play");
		}
		
		override
		{
			void Init()
			{
				Battle.ThisPlayer = new Player(this.LoadPlayerData(), false);
				this._LastAttack = new Button(null, null, sfVector2f(160, (18 * 6) + 40), sfVector2f(Util.CenterX(160), 6), TextOptions(18, ""));
			}
			
			void Draw(sfRenderWindow* window)
			{
				this._LastAttack.Draw(window);
				Battle.Draw(window);
			}
			
			void Update(sfTime gameTime)
			{
				this._LastAttack.Text = format(
					"Health:  \t%s\n"~
					"PDefence: %s\n"~
					"MDefence: %s\n"~
					"PAttack: \t%s\n"~
					"MAttack: \t%s\n"~
					"\nDamage:\t%s\n",
					
					Battle.LastAttack.Damage.Health,
					Battle.LastAttack.Damage.PhysicalDefence,
					Battle.LastAttack.Damage.MagicalDefence,
					Battle.LastAttack.Damage.PhysicalAttack,
					Battle.LastAttack.Damage.MagicalAttack,
					Battle.LastAttack.TotalDamage).toUTF32();

				this._LastAttack.Update(gameTime);
				Battle.Update(gameTime);
			}
		}
	}
}