﻿module Game.Player;

private
{
	import std.utf;
}

public
{
	import Game.Drawable, Game.Loader, Game.Config, Game.Keyboard, Game.Mouse, Game.Item, Game.Button, Game.Network;
}

struct StatInfo
{
	/// How much health the player has/Item adds.
	int	Health;

	/// How much damage can be absorbed from physical attacks.
	int	PhysicalDefence;

	/// How much damage can be absorbed from magical attacks.
	int	MagicalDefence;

	/// How much damage is added to physical attacks.
	int	PhysicalAttack;

	/// How much damage is added to magical attacks.
	int	MagicalAttack;

	void TakeFromStats(ref StatInfo stats)
	{
		stats.Health -= Health;
		stats.PhysicalDefence -= PhysicalDefence;
		stats.MagicalDefence -= MagicalDefence;
		stats.PhysicalAttack -= PhysicalAttack;
		stats.MagicalAttack -= MagicalAttack;
	}
}

struct PlayerGear
{
	Item	Helmet, Chest, Greaves, Boots, Weapon;

	void Draw(sfRenderWindow* window)
	{
		Helmet.Draw(window);
		Chest.Draw(window);
		Greaves.Draw(window);
		Boots.Draw(window);
		Weapon.Draw(window);
	}

	void Update(sfTime gameTime)
	{
		Helmet.Update(gameTime);
		Chest.Update(gameTime);
		Greaves.Update(gameTime);
		Boots.Update(gameTime);
		Weapon.Update(gameTime);
	}

	void AddToStats(ref StatInfo stat)
	{
		Item[] X = [Helmet, Chest, Greaves, Boots, Weapon];
		foreach(x; X)
		{
			stat.Health 			+= x.Bonuses.Health;
			stat.PhysicalDefence 	+= x.Bonuses.PhysicalDefence;
			stat.MagicalDefence 	+= x.Bonuses.MagicalDefence;
			stat.PhysicalAttack 	+= x.Bonuses.PhysicalAttack;
			stat.MagicalAttack 		+= x.Bonuses.MagicalAttack;
		}
	}
}
/*
EROR
	- 
		- Placeholder textures
		- Networking
		- Item outfitter
		- Health system working
		- Losing screen working
		- Damage system in place
		- Factor in P/MDefence and P/MAttack
*/

class Player : Drawable
{
	private
	{
		sfSprite*	_Sprite;
		StatInfo	_Stats;
		StatInfo	_CurrentStats; // This one is the one that changes throughout the battle
		PlayerGear	_Gear;
	
		bool		_Enemy;

		Button		_StatWindow;
		Button[]	_Skills;

		uint[5]		_GearID;

		void X(Button sender, GameState state){}

		void CastSkill(Button sender, GameState state)
		{
			if(this._Gear.Weapon.AnimatingAttack)
			{
				Util.LogSender = "Player::CastSkill";
				Util.Log("Denying request to cast skill -- already casting one.");
				return;
			}

			with(this._Gear.Weapon)
			{
				for(uint i = 0; i < Attacks.length; i++)
				{
					if(Attacks[i].Name.toUTF32() == sender.Text)
					{
						Network.Send([i], Opcode.Attack);
						SetAttack(i);
						DoAttack();
						return;
					}
				}
			}
		}
	}

	public
	{
		this(uint[5] gearID, bool enemy)
		{
			this._GearID = gearID;

			// FIXME: Place holder
			this._Stats = StatInfo(50, 5, 5, 10, 10);

			auto Temp = enemy ? Config.EnemyPosition : Config.PlayerPosition;

			this._Enemy = enemy;
			this._Gear = PlayerGear(
				ItemLoader.Construct(gearID[0], Temp, this),
				ItemLoader.Construct(gearID[1], Temp, this),
				ItemLoader.Construct(gearID[2], Temp, this),
				ItemLoader.Construct(gearID[3], Temp, this),
				ItemLoader.Construct(gearID[4], Temp, this)
				);
			this._Gear.AddToStats(this._Stats);
			this._CurrentStats = this._Stats;	
			this.Init();

			// This sorts out some issues attacks have
			foreach(i; 0..this._Gear.Weapon.Attacks.length)
			{
				this._Gear.Weapon.SetAttack(i);
				this._Gear.Weapon.DoAttack();
				this._Gear.Weapon.GetAttack(i).Reset();
			}
		}

		override
		{
			void Init()
			{
				this._Sprite = sfSprite_create();

				auto Tahn = Loader.LoadTexture(Config.Player.Texture);
				sfSprite_setTexture(this._Sprite, Tahn, true);

				if(this._Enemy)
				{
					this.Position = Config.EnemyPosition;
					sfSprite_setScale(this._Sprite, sfVector2f(-1, 1));
				}
				else
				{
					this.Position = Config.PlayerPosition;
				}

				this._StatWindow = new Button(null, &this.X, sfVector2f(200, (18 * 5) + 80), sfVector2f(this.Position.x + (this.IsEnemy ? -100 : 0), this.Position.y + sfTexture_getSize(Tahn).y + 30), TextOptions(18, ""));

				uint Count = 0;
				foreach(attack; this._Gear.Weapon.Attacks)
				{
					this._Skills ~= new Button(null, &this.CastSkill, sfVector2f(150, 50), sfVector2f(Util.CenterX(150), Util.CenterY(50) + ((Count * 50) + 6)), TextOptions(20, attack.Name.toUTF32()));
					Count += 1;
				}
			}

			void Draw(sfRenderWindow* window)
			{
				sfRenderWindow_drawSprite(window, this._Sprite, null);
				this._Gear.Draw(window);
				this._StatWindow.Draw(window);

				if(!this._Enemy && Battle.OurTurn)
				{
					foreach(skill; this._Skills) skill.Draw(window);
				}
			}

			void Update(sfTime gameTime)
			{
				void UpdateText(StatInfo info)
				{
					this._StatWindow.Text = format(
						"Health:  \t%d\n"~
						"PDefence: %d\n"~
						"MDefence: %d\n"~
						"PAttack: \t%d\n"~
						"MAttack: \t%d\n",
						
						info.Health,
						info.PhysicalDefence,
						info.MagicalDefence,
						info.PhysicalAttack,
						info.MagicalAttack).toUTF32();
				}

				this._Gear.Update(gameTime);
				this._StatWindow.Update(gameTime);


				bool Boobs = false;
				uint Count = 0;
				if(!this._Enemy && Battle.OurTurn)
				{
					foreach(skill; this._Skills)
					{
						skill.Update(gameTime);

						if(skill.IsOver)
						{
							Boobs = true;
							UpdateText(this.Gear.Weapon.Attacks[Count].Damage);
							break;
						}
						else
						{
							Count++;
						}
					}
				}

				if(!Boobs)
				{
					UpdateText(this._CurrentStats);
				}
			}
		}

		void DoDamage(LastAttackInfo damage)
		{
			this._CurrentStats.Health -= damage.TotalDamage;
			this._CurrentStats.PhysicalDefence += damage.Damage.PhysicalDefence;
			this._CurrentStats.MagicalDefence += damage.Damage.MagicalDefence;
		}

		/// Get the player's stats(Can be altered).
		@property
		ref StatInfo Stats()
		{
			return this._Stats;
		}

		/// Get the player's gear
		@property
		PlayerGear Gear()
		{
			return this._Gear;
		}

		/// Get the player's position.
		@property
		sfVector2f Position()
		{
			return sfSprite_getPosition(this._Sprite);
		}

		/// Set the player's position.
		@property
		void Position(sfVector2f position)
		{
			sfSprite_setPosition(this._Sprite, position);
		}

		@property
		bool IsEnemy()
		{
			return this._Enemy;
		}

		@property
		uint[5] GearID()
		{
			return this._GearID;
		}
	}
}