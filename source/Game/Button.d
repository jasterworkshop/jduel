﻿module Game.Button;

public
{
	import Game.Drawable, Game.GameState, Game.Config, Game.Mouse, Game.Util, Game.TextOptions, Game.Loader;
}

alias ButtonEvent = void delegate(Button sender, GameState senderState);

// I can't be bothered making a Control class for something so easy to make, I won't be making a container either.
class Button : Drawable
{
	private
	{
		GameState			_State;
		ButtonEvent			_Event;

		sfRectangleShape*	_Sprite;
		sfVector2f			_Size;
		sfVector2f			_Position;

		bool				_Lock = false;

		TextOptions			_Options;
		sfText*				_Text;
		sfText*				_TextOutline;

		dstring				_TextT;
	}

	public
	{
		this(GameState parent, ButtonEvent event, sfVector2f size, sfVector2f position, TextOptions text)
		{
			this._State 	= parent;
			this._Event 	= event;
			this._Size 		= size;
			this._Position 	= position;
			this._Options 	= text;

			this.Init();
			this.Position = position;
		}

		~this()
		{
			sfRectangleShape_destroy(this._Sprite);
			sfText_destroy(this._Text);
			sfText_destroy(this._TextOutline);
		}

		override
		{
			void Init()
			{
				this._Sprite = sfRectangleShape_create();
				sfRectangleShape_setSize(this._Sprite, this._Size);
				sfRectangleShape_setOutlineThickness(this._Sprite, 4);
				sfRectangleShape_setFillColor(this._Sprite, Config.Button.Fill);
				sfRectangleShape_setOutlineColor(this._Sprite, Config.Button.Outline);

				this.Text = this._Options.Text;
			}

			void Draw(sfRenderWindow* window)
			{
				sfRenderWindow_drawRectangleShape(window, this._Sprite, null);
				sfRenderWindow_drawText(window, this._TextOutline, null);
				sfRenderWindow_drawText(window, this._Text, null);
			}

			void Update(sfTime gameTime)
			{
				sfVector2i _MousePos = Mouse.Position;
				sfVector2f MousePos = sfVector2f(_MousePos.x, _MousePos.y);

				if(this.IsOver)
				{
					if(Mouse.LeftPressed)
					{
						sfRectangleShape_setFillColor(this._Sprite, Util.Tint(Config.Button.Fill, sfColor(128, 128, 128, 255)));

						if(!this._Lock && this._Event !is null)
						{
							this._Event(this, this.ParentState);
						}

						this._Lock = true;
					}
					else
					{
						sfRectangleShape_setFillColor(this._Sprite, Util.Tint(Config.Button.Fill, sfColor(164, 164, 164, 255)));
					}

					if(this._Lock)
					{
						this._Lock = Mouse.LeftPressed;
					}
				}
				else
				{
					sfRectangleShape_setFillColor(this._Sprite, Config.Button.Fill);
				}
			}
		}

		/// Get the GameState that the button belongs to.
		@property
		GameState ParentState()
		{
			return this._State;
		}

		/// Get the size of the button.
		@property
		sfVector2f Size()
		{
			return this._Size;
		}

		@property
		sfVector2f Position()
		{
			return this._Position;
		}

		@property
		void Position(sfVector2f position)
		{
			this._Position = Position;
			sfRectangleShape_setPosition(this._Sprite, position);

			sfFloatRect TextRect = sfText_getGlobalBounds(this._Text);

			sfText_setPosition(this._Text, sfVector2f(
					(this.Position.x + (this.Size.x / 2)) - (TextRect.width / 2),
					(this.Position.y + (this.Size.y / 2)) - (TextRect.height / 2)
					));

			sfText_setPosition(this._TextOutline, sfText_getPosition(this._Text));
		}

		@property
		bool IsOver()
		{
			sfVector2i _MousePos = Mouse.Position;
			sfVector2f MousePos = sfVector2f(_MousePos.x, _MousePos.y);

			// You can tell I've given up on readability by this point
			if(MousePos.x >= this.Position.x && MousePos.x <= this.Position.x + this.Size.x &&
				MousePos.y >= this.Position.y && MousePos.y <= this.Position. y + this.Size.y)
				return true;
			else
				return false;
		}

		@property
		void Text(dstring text)
		{			
			this._Text = sfText_create();
			sfText_setFont(this._Text, Loader.DrawingFont);
			sfText_setColor(this._Text, this._Options.Colour);
			sfText_setCharacterSize(this._Text, this._Options.Size);
			sfText_setUnicodeString(this._Text, cast(const(uint)*)(text ~ '\0').ptr);

			sfFloatRect TextRect = sfText_getGlobalBounds(this._Text);
			sfText_setPosition(this._Text, sfVector2f(
					(this.Position.x + (this.Size.x / 2)) - (TextRect.width / 2),
					(this.Position.y + (this.Size.y / 2)) - (TextRect.height / 2)
					));
			
			this._TextOutline = sfText_copy(this._Text);
			sfText_setColor(this._TextOutline, sfBlack);

			this._TextT = text;
		}

		@property
		dstring Text()
		{
			return this._TextT;
		}
	}
}

