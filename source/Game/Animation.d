﻿module Game.Animation;

public
{
	import Game.Drawable, Game.Loader, Game.Config, Game.Keyboard, Game.Mouse;
}

struct AnimationInfo
{
	/// Width of 1 frame
	uint	FrameWidth;

	/// Height of 1 frame.
	uint	FrameHeight;

	/// How many frames their are. (Can be calculated: Count = ImageWidth / FrameWidth)
	uint	FrameCount;

	/// How many Milliseconds before the next frame.
	uint	FrameTimer;

	/// Should the animation loop after drawing the last frame?
	bool	Repeatable;

	/// Path to the texture.
	string	Texture;
}

class Animation : Drawable
{
	private
	{
		AnimationInfo	_Info;
		sfSprite*		_Sprite;
		uint			_Frame;
		uint			_Time;
	}

	public
	{
		this(AnimationInfo info)
		{
			this._Info = info;
			this.Init();
		}

		~this()
		{
			if(this._Sprite !is null)
			{
				sfSprite_destroy(this._Sprite);
			}
		}

		void Reset()
		{
			this._Frame = 0;
			this._Time = 0;
			sfSprite_setTextureRect(this._Sprite, sfIntRect(0, 0, this._Info.FrameWidth, this._Info.FrameHeight));
		}

		override
		{
			void Init()
			{
				this._Sprite = sfSprite_create();

				auto Text = Loader.LoadTexture(this._Info.Texture);
				this._Info.FrameCount = (sfTexture_getSize(Text).x / this._Info.FrameWidth);
				sfSprite_setTexture(this._Sprite, Text, true);
				sfSprite_setTextureRect(this._Sprite, sfIntRect(0, 0, this._Info.FrameWidth, this._Info.FrameHeight));

				Util.LogSender = "Animation::Init";
				Util.Log("Animation created, final Info: %s", this._Info);
			}

			void Draw(sfRenderWindow* window)
			{
				sfRenderWindow_drawSprite(window, this._Sprite, null);
			}

			void Update(sfTime gameTime)
			{
				this._Time += sfTime_asMilliseconds(gameTime);

				if(this._Time >= this._Info.FrameTimer)
				{
					if(this._Frame == this._Info.FrameCount - 1)
					{
						if(this._Info.Repeatable)
						{
							this._Frame = 0;
						}
					}
					else
					{
						this._Frame++;
					}

					sfSprite_setTextureRect(this._Sprite, sfIntRect(this._Frame * this._Info.FrameWidth, 0, this._Info.FrameWidth, this._Info.FrameHeight));
				}
			}
		}

		void Flip()
		{
			sfSprite_setScale(this._Sprite, sfVector2f(-1, 1));
		}

		@property
		void Position(sfVector2f position)
		{
			sfSprite_setPosition(this._Sprite, position);
		}
		
		@property
		sfVector2f Position()
		{
			return sfSprite_getPosition(this._Sprite);
		}
	}
}

