﻿module Game.TextOptions;

private
{
	import derelict.sfml2.graphics, derelict.sfml2.system;
}

struct TextOptions
{
	uint	Size;
	sfColor	Colour;
	dstring	Text;

	this(uint size, dstring text, sfColor colour = sfColor(0xFF, 0xFF, 0xFF, 0xFF))
	{
		this.Size 	= size;
		this.Text 	= text;
		this.Colour = colour;
	}
}

