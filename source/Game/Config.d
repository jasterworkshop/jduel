﻿module Game.Config;

private
{
	import derelict.sfml2.graphics : sfColor;
	import derelict.sfml2.window : sfVideoMode;
	import derelict.sfml2.system;
}

static class Config
{
	public static
	{
		struct AssetDirectories
		{
			static
			{
				string Textures = "Assets/Textures/";
				string Items	= "Assets/Items/";
			}
		}

		struct Window
		{
			static
			{
				string 		Title 		= "Jaster Duel\0";
				sfVideoMode VideoMode	= sfVideoMode(800, 460, 32);
			}
		}

		struct Button
		{
			static
			{
				sfColor	Fill	= sfColor(0x6B, 0x70, 0x78, 0xFF);
				sfColor Outline	= sfColor(0x55, 0x59, 0x5E, 0xFF);
			}
		}

		struct Player
		{
			static
			{
				string	Texture 	= "Entities/Player.png";
			}
		}

		sfVector2f 	EnemyPosition 	= sfVector2f(700, 100);
		sfVector2f	PlayerPosition	= sfVector2f(100, 100);

		string 		MainFont 		= "Assets/Arial.ttf\0";
		ushort		Port			= 7777;

		string		PlayerData		= "Player.json";
	}
}