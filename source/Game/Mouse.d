﻿module Game.Mouse;

private
{
	import derelict.sfml2.window;

	import Game.Util;
}

static class Mouse
{
	private static
	{
		bool		_LeftDown 	= false;
		bool		_RightDown 	= false;
		bool		_MiddleDown = false;

		sfVector2i _Position;
	}

	public static
	{
		/// Handles mouse input
		void HandleMouseInput(sfEvent e)
		{
			if(e.type == sfEvtMouseButtonPressed)
			{
				switch(e.mouseButton.button)
				{
					case sfMouseLeft: 	this._LeftDown = true; break;
					case sfMouseRight: 	this._RightDown = true; break;
					case sfMouseMiddle: this._MiddleDown = true; break;
					default: throw new Exception("");
				}
			}
			else if(e.type == sfEvtMouseButtonReleased)
			{
				switch(e.mouseButton.button)
				{
					case sfMouseLeft: 	this._LeftDown = false; break;
					case sfMouseMiddle: this._MiddleDown = false; break;
					case sfMouseRight: 	this._RightDown = false; break;
					default: throw new Exception("");
				}
			}
		}

		/// Sets the mouse's position
		void HandleMousePosition(sfRenderWindow* window)
		{
			this._Position = sfMouse_getPositionRenderWindow(window);
		}

		/// Reset all flags
		void Clear()
		{
			Util.LogSender = "Mouse::Clear";
			Util.Log("Clearing flags.");

			this._LeftDown 	= false;
			this._RightDown = false;
			this._MiddleDown = false;
		}

		/// Get whether the right mouse button is currently down.
		@property
		bool RightPressed()
		{
			return this._RightDown;
		}

		/// Get whether the left mouse button is currently down.
		@property
		bool LeftPressed()
		{
			return this._LeftDown;
		}

		/// Get whether the middle mouse button is currently down.
		@property
		bool MiddlePressed()
		{
			return this._MiddleDown;
		}

		/// Get the mouse's position, relative to the window.
		@property
		sfVector2i Position()
		{
			return this._Position;
		}
	}
}

