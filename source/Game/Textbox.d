﻿module Game.Textbox;

public
{
	import Game.Drawable, Game.GameState, Game.Config, Game.Mouse, Game.Util, Game.TextOptions, Game.Loader, Game.Keyboard;
}

// I can't be bothered making a Control class for something so easy to make, I won't be making a container either.
// FIXME: Blehhhh, Textbox and control are just a mess of copy-paste of eachother, don't bother fixing it if it works, because you'll just break something.
class TextBox : Drawable
{
	private
	{
		GameState			_State;
		
		sfRectangleShape*	_Sprite;
		sfVector2f			_Size;
		sfVector2f			_Position;

		bool				_Selected = false;
		bool				_IsPassword = false;
		dstring				_Input = ""d;
		dchar[]				_PasswordString;
		
		TextOptions			_Options;
		sfText*				_Text;
		sfText*				_TextOutline;
	}
	
	public
	{
		this(GameState parent, sfVector2f size, sfVector2f position, TextOptions text)
		{
			this._State 	= parent;
			this._Size 		= size;
			this._Position 	= position;
			this._Options 	= text;
			
			this.Init();
			this.Position = position;
		}
		
		~this()
		{
			sfRectangleShape_destroy(this._Sprite);
			sfText_destroy(this._Text);
			sfText_destroy(this._TextOutline);
		}
		
		override
		{
			void Init()
			{
				this._Sprite = sfRectangleShape_create();
				sfRectangleShape_setSize(this._Sprite, this._Size);
				sfRectangleShape_setOutlineThickness(this._Sprite, 4);
				sfRectangleShape_setFillColor(this._Sprite, Config.Button.Fill);
				sfRectangleShape_setOutlineColor(this._Sprite, Config.Button.Outline);
				
				this._Text = sfText_create();
				sfText_setFont(this._Text, Loader.DrawingFont);
				sfText_setColor(this._Text, this._Options.Colour);
				sfText_setCharacterSize(this._Text, this._Options.Size);
				
				this._TextOutline = sfText_copy(this._Text);
				sfText_setColor(this._TextOutline, sfBlack);
			}
			
			void Draw(sfRenderWindow* window)
			{
				sfRenderWindow_drawRectangleShape(window, this._Sprite, null);
				sfRenderWindow_drawText(window, this._TextOutline, null);
				sfRenderWindow_drawText(window, this._Text, null);
			}
			
			void Update(sfTime gameTime)
			{
				sfVector2i _MousePos = Mouse.Position;
				sfVector2f MousePos = sfVector2f(_MousePos.x, _MousePos.y);

				if(Mouse.LeftPressed)
				{						
					if(MousePos.x >= this.Position.x && MousePos.x <= this.Position.x + this.Size.x &&
						MousePos.y >= this.Position.y && MousePos.y <= this.Position. y + this.Size.y)
					{
						this._Selected = true;
					}
					else
					{
						this._Selected = false;
					}
				}

				if(this._Selected)
				{
					foreach(dchr; Keyboard.Pressed)
					{
//						import std.stdio, std.utf;
//						writefln("Char: %s | Num: %d | Test: %s", dchr, dchr, cast(ubyte[])(cast(void[])[dchr]));

						if(dchr == '\b')
						{
							if(this._Input.length > 0)
							{
								this._Input.length = (this._Input.length - 1);
							}
						}
						else if(dchr == 13) // Enter key, do nothing
						{

						}
						else
						{
							this._Input ~= dchr;
						}
					}

					if(this._IsPassword)
					{
						this._PasswordString.length = this._Input.length + 1;
						this._PasswordString[] = '*';
						this._PasswordString[$ - 1] = '\0';

						sfText_setUnicodeString(this._Text, cast(const(uint)*)this._PasswordString.ptr); 
					}
					else
					{
						sfText_setUnicodeString(this._Text, cast(const(uint)*)(this._Input ~ '\0').ptr);
					}

					// Limit check
					sfFloatRect TextRect = sfText_getGlobalBounds(this._Text);					
					while((sfText_getPosition(this._Text).x + TextRect.width) > (this.Position.x + this.Size.x))
					{
						if(this._IsPassword)
						{
							this._Input.length = this._Input.length - 1;
							this._PasswordString.length = this._PasswordString.length - 1;
							sfText_setUnicodeString(this._Text, cast(const(uint)*)this._PasswordString.ptr);
						}
						else
						{
							this._Input.length = this._Input.length - 1;
							sfText_setUnicodeString(this._Text, cast(const(uint)*)(this._Input ~ '\0').ptr);
						}

						TextRect = sfText_getGlobalBounds(this._Text);
					}

					sfText_setUnicodeString(this._TextOutline, sfText_getUnicodeString(this._Text));
					sfRectangleShape_setFillColor(this._Sprite, Util.Tint(Config.Button.Fill, sfColor(220, 220, 220, 255)));
				}
				else
				{
					sfRectangleShape_setFillColor(this._Sprite, Config.Button.Fill);
				}
			}
		}
		
		/// Get the GameState that the button belongs to.
		@property
		GameState ParentState()
		{
			return this._State;
		}
		
		/// Get the size of the button.
		@property
		sfVector2f Size()
		{
			return this._Size;
		}
		
		@property
		sfVector2f Position()
		{
			return this._Position;
		}
		
		@property
		void Position(sfVector2f position)
		{
			this._Position = Position;
			sfRectangleShape_setPosition(this._Sprite, position);
			
			sfFloatRect TextRect = sfText_getGlobalBounds(this._Text);
			
			sfText_setPosition(this._Text, sfVector2f(
					this.Position.x + 5,
					(this.Position.y + (this.Size.y / 2)) - (this._Options.Size / 2)
					));
			
			sfText_setPosition(this._TextOutline, sfText_getPosition(this._Text));
		}
	}
}

