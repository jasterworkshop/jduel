﻿module Game.Network;

private
{
	import std.socket;
	import std.bitmanip;
	import std.traits;
	import std.datetime;
}

public
{
	import Game.Config, Game.Util, Game.Battle;
}

enum Opcode : ubyte
{
	Test,
	Message, 		// Payload = char[]
	CreateEnemy, 	// Payload = EnemyInfo[1]
	Attack			// Payload = uint[1]
}

struct EnemyInfo
{
	uint[5]	Gear;
}

struct IncomingData
{
	void[]	Data;
	Opcode	Operation;

	T[] To(T)()
	{
		return cast(T[])this.Data;
	}
}

static class Network
{
	private static
	{
		Socket	_Us;
		Socket	_Them;

		SocketSet _Set;
	}

	public static
	{
		void Connect(string ip)
		{
			Network._Set = new SocketSet();

			Util.Log("Connecting to '%s'.", ip);
			Network._Us = new TcpSocket();
			Network._Us.connect(new InternetAddress(ip, Config.Port));
			Network._Them = Network._Us;

			Util.Log("Connected, performing send/recieve test.");

			if(Network.Recieve().Operation != Opcode.Test)
			{
				Util.Log("Send/recieve test failed.");
			}
			Network.Send!ushort([0x01], Opcode.Test);
			
			Network._Set.add(Network._Them);
			Util.Log("Successfully connected to enemy!");

			Battle.SetTurn(false);
		}

		~this()
		{
			if(Network._Us !is null) Network._Us.close();
			if(Network._Them !is null) Network._Them.close();
		}

		void Host()
		{
			Network._Set = new SocketSet();

			Util.Log("Hosting game.");

			Network._Us = new TcpSocket();
			Network._Us.bind(new InternetAddress(InternetAddress.ADDR_ANY, Config.Port));

			Network._Us.listen(1);
			Util.Log("Awaiting connection (Game will stop responding).");

			Network._Them = Network._Us.accept();

			Util.Log("Connection accepted, performing send/recieve test.");

			Network.Send!ushort([0x01], Opcode.Test);
			if(Network.Recieve().Operation != Opcode.Test)
			{
				Util.Log("Send/recieve test failed.");
			}

			Network._Set.add(Network._Them);
			Util.Log("Successfully connected to enemy!");

			Battle.SetTurn(true);
		}

		void Send(T)(T[] data, Opcode op)
		{
			if(Network._Them is null) return;

			ptrdiff_t Size = (data.length * T.sizeof);
			Util.Log("Request to send %d bytes of '%s', operation '%s'.", Size, fullyQualifiedName!T, op);

			ubyte[] ToSend;
			ToSend.length = (Opcode.sizeof + Size + uint.sizeof);

			ToSend[0] = cast(ubyte)op;
			ToSend[1..5] = (cast(ubyte*)&Size)[0..uint.sizeof];
			ToSend[5..$] = cast(ubyte[])(cast(void[])data);

			Util.Log("Final size of packet = %d", ToSend.length);
			Util.Log("Writing to enemy.");

			ptrdiff_t Sent = Network._Them.send(ToSend);
			Util.Log("Sent %d bytes out of %d bytes.", Sent, ToSend.length);
		}

		IncomingData Recieve()
		{
			if(Network._Them is null) return IncomingData(null, Opcode.Test);

			Util.Log("Request to recieve packet.");
			IncomingData Data;

			Util.Log("Recieving header(5 bytes).");
			ubyte[5] Header;
			size_t Got = Network._Them.receive(Header);

			Data.Operation = cast(Opcode)Header[0];
			Data.Data.length = (cast(uint[])(cast(void[])Header[1..$]))[0];

			Util.Log("Read %d bytes out of 5 bytes.", Got);
			Util.Log("Receiving packet data.");
			Got = Network._Them.receive(Data.Data);

			Util.Log("Read %d bytes out of %d bytes.", Got, Data.Data.length);
			Util.Log("Recieved packet that was %d bytes large, describing the '%s' operation.", Got + Header.length, Data.Operation);

			return Data;
		}

		void Update()
		{
			if(Network._Them is null) return;

			if(Socket.select(Network._Set, null, null, seconds(0)) >= 1) // If they've sent data
			{
				auto Data = Network.Recieve();

				switch(Data.Operation)
				{
					case Opcode.Message:
						Util.LogSender = "Network::Update";
						Util.Log("<Enemy>: '%s'.", Data.To!char);
						break;

					case Opcode.Attack:
						Battle.EnemyPlayer.Gear.Weapon.SetAttack(Data.To!uint[0]);
						Battle.EnemyPlayer.Gear.Weapon.DoAttack();
						break;

					case Opcode.CreateEnemy:
						Battle.EnemyPlayer = new Player(Data.To!EnemyInfo[0].Gear, true);
						break;

					default: break;
				}
			}

			if(!Network._Set.isSet(Network._Them))
			{
				Network._Set.add(Network._Them);
			}
		}

		@property
		bool Connected()
		{
			return Network._Them is null;
		}
	}
}