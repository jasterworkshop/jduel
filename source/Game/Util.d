﻿module Game.Util;

private
{
	import std.stdio : writefln;

	import Game.Config;
}

public
{
	import derelict.sfml2.audio, derelict.sfml2.graphics, derelict.sfml2.system, derelict.sfml2.window;
}

static class Util
{
	private static
	{
		string _Sender = "";
	}

	public static
	{
		/// Initialise Derelict
		void InitDerelict()
		{
			Util.LogSender = "Util.InitDerelict";
			Util.Log("Initialising Derelict sub-systems.");

			DerelictSFML2Audio.load();
			DerelictSFML2Window.load();
			DerelictSFML2System.load();
			DerelictSFML2Graphics.load();
		}

		void Log(T...)(string format, T args)
		{
			writefln("[%s] " ~ format, Util._Sender, args);
		}

		@property
		void LogSender(string sender)
		{
			Util._Sender = sender;
		}

		sfColor Tint(sfColor colour, sfColor tint)
		{
			return sfColor(colour.r * tint.r / 255, colour.g * tint.g / 255, colour.b * tint.b / 255, colour.a);
		}

		float CenterX(float widthOfThing)
		{
			return ((Config.Window.VideoMode.width / 2) - (widthOfThing / 2));
		}

		float CenterY(float heightOfThing)
		{
			return ((Config.Window.VideoMode.height / 2) - (heightOfThing / 2));
		}
	}
}