﻿module Game.Battle;

public
{
	import Game.Util, Game.Config, Game.Player, Game.Network, Game.Item;
}

struct LastAttackInfo
{
	string		Name;
	StatInfo	Damage;
	int			TotalDamage;
}

static class Battle
{
	private static
	{
		Player			_ThisPlayer;
		Player			_EnemyPlayer;

		bool			_OurTurn;

		LastAttackInfo	_LastAttack;
	}

	public static
	{
		void Draw(sfRenderWindow* window)
		{
			if(Battle._EnemyPlayer !is null) Battle._EnemyPlayer.Draw(window);
			if(Battle._ThisPlayer !is null) Battle._ThisPlayer.Draw(window);
		}

		void Update(sfTime gameTime)
		{
			if(Battle._EnemyPlayer !is null) Battle._EnemyPlayer.Update(gameTime);
			if(Battle._ThisPlayer !is null) Battle._ThisPlayer.Update(gameTime);
		}

		void ProcessAttack(uint attackID)
		{
			void ProccessPlayer(Player player1, Player player2)
			{
				StatInfo FinalDamage;
				Attack TheAttack = player1.Gear.Weapon.GetAttack(attackID);
				FinalDamage.Health = TheAttack.Damage.Health;
				FinalDamage.PhysicalDefence = TheAttack.Damage.PhysicalDefence;
				FinalDamage.MagicalDefence = TheAttack.Damage.MagicalDefence;
				
				if(player2.Stats.PhysicalDefence < TheAttack.Damage.PhysicalAttack + player1.Stats.PhysicalAttack)
				{
					FinalDamage.PhysicalAttack = (TheAttack.Damage.PhysicalAttack - player2.Stats.PhysicalDefence) + player1.Stats.PhysicalAttack; 
				}
				if(player2.Stats.MagicalDefence < TheAttack.Damage.MagicalAttack + player1.Stats.MagicalAttack)
				{
					FinalDamage.MagicalAttack = (TheAttack.Damage.MagicalAttack - player2.Stats.MagicalDefence) + player1.Stats.MagicalAttack; 
				}

				this._LastAttack = LastAttackInfo(TheAttack.Name, FinalDamage, FinalDamage.Health + FinalDamage.MagicalAttack + FinalDamage.PhysicalAttack);
				player2.DoDamage(this.LastAttack);
			}

			if(Battle.OurTurn)
			{
				ProccessPlayer(Battle.ThisPlayer, Battle.EnemyPlayer);
			}
			else
			{
				ProccessPlayer(Battle.EnemyPlayer, Battle.ThisPlayer);
			}

			Battle._OurTurn = !Battle._OurTurn;	
		}

		void SendPlayerToEnemy()
		{
			Network.Send([EnemyInfo(Battle.ThisPlayer.GearID)], Opcode.CreateEnemy);
		}

		@property
		void ThisPlayer(Player p)
		{
			Battle._ThisPlayer = p;
		}

		@property
		Player ThisPlayer()
		{
			return Battle._ThisPlayer;
		}

		@property
		void EnemyPlayer(Player p)
		{
			Battle._EnemyPlayer = p;
		}

		@property
		Player EnemyPlayer()
		{
			return Battle._EnemyPlayer;
		}

		void SetTurn(bool turn)
		{
			Battle._OurTurn = turn;
		}

		@property
		bool OurTurn()
		{
			return Battle._OurTurn;
		}

		@property
		LastAttackInfo LastAttack()
		{
			return this._LastAttack;
		}
	}
}