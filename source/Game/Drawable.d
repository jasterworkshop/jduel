﻿module Game.Drawable;

public
{
	import derelict.sfml2.graphics, derelict.sfml2.window, derelict.sfml2.system;
}

abstract class Drawable
{
	public abstract
	{
		void Init();
		void Draw(sfRenderWindow* window);
		void Update(sfTime gameTime);
	}
}

